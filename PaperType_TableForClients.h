//---------------------------------------------------------------------------

#ifndef PaperType_TableForClientsH
#define PaperType_TableForClientsH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.Grids.hpp>
//---------------------------------------------------------------------------
class TPTCform : public TForm
{
__published:	// IDE-managed Components
	TDBGrid *DBGrid1;
private:	// User declarations
public:		// User declarations
	__fastcall TPTCform(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TPTCform *PTCform;
//---------------------------------------------------------------------------
#endif
