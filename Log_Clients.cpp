// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Log_Clients.h"
#include "All_Connections_Invis.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TOSAForm *OSAForm;

// ---------------------------------------------------------------------------
__fastcall TOSAForm::TOSAForm(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TOSAForm::FormShow(TObject *Sender) {
	DM->ADOLog->Open();
}
// ---------------------------------------------------------------------------

void __fastcall TOSAForm::FormClose(TObject *Sender, TCloseAction &Action) {
	DM->ADOLog->Close();
}
// ---------------------------------------------------------------------------
