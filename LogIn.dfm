object LoginForm: TLoginForm
  Left = 0
  Top = 0
  Caption = 'LoginForm'
  ClientHeight = 212
  ClientWidth = 169
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 64
    Top = 24
    Width = 27
    Height = 13
    Caption = 'LogIn'
  end
  object Label2: TLabel
    Left = 56
    Top = 93
    Width = 46
    Height = 13
    Caption = 'Password'
  end
  object eLog: TEdit
    Left = 24
    Top = 51
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object ePass: TEdit
    Left = 24
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object LogB: TButton
    Left = 48
    Top = 155
    Width = 73
    Height = 25
    Caption = 'Go!'
    TabOrder = 2
    OnClick = LogBClick
  end
end
