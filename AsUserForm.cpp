// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AsUserForm.h"
#include "All_Connections_Invis.h"
#include "PaperType_TableForClients.h"
#include "PaperSize_TableForClients.h"
#include "Filials_TableForClients.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TUserOrderForm *UserOrderForm;

// ---------------------------------------------------------------------------
__fastcall TUserOrderForm::TUserOrderForm(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TUserOrderForm::FormShow(TObject *Sender) {
	DM->ADOOrder->Open();
	DM->ADOPaper_type->Open();
	DM->ADOTClient->Open();
	DM->ADOOrder->Insert();
}
// ---------------------------------------------------------------------------

// поиск по имени
// позволяет искать в режиме "реального времени"
// В идеале - так делать нельзя, т.к. конфиденциальность и всё такое
// Не очень логично, но коли требуется демонстрация такой функции, ей быть
// "Мыши плакали, кололись, но продолжали есть кактус.." - 2
// и таких - три текстбокса...

void __fastcall TUserOrderForm::ePoiskChange(TObject *Sender) {
	if (ePoisk->Text != "") {
		DM->ADOTClient->Filter = "last_name LIKE '" + ePoisk->Text + "*'";
		DM->ADOTClient->Filtered = true;
	}

}
// ---------------------------------------------------------------------------

// сделать заказ
void __fastcall TUserOrderForm::OrderBClick(TObject *Sender) {
	if (UserOrderForm->DBKolvo->Text == "" ||
		StrToInt(UserOrderForm->DBKolvo->Text) < 1) {
		ShowMessage(
			"Количество не может быть отрицательным, нулевым или пустым");
	}
	else {
		DM->ADOOrder->FieldByName("id_clients")->AsInteger =
			DM->ADOTClient->FieldByName("id")->AsInteger;
		DM->ADOOrder->Insert();
	}
}
// ---------------------------------------------------------------------------

// показать типы бумаги
void __fastcall TUserOrderForm::ButtonPTClick(TObject *Sender) {
	PTCform->ShowModal();
}
// ---------------------------------------------------------------------------

// показать рамерную сетку для бумаги
void __fastcall TUserOrderForm::ButtonPSClick(TObject *Sender) {
	PSCform->ShowModal();
}
// ---------------------------------------------------------------------------

// показать филиалы
void __fastcall TUserOrderForm::ButtonFClick(TObject *Sender) {
	FCform->ShowModal();
}
// ---------------------------------------------------------------------------

// процедура, расчет стоимости печати
void __fastcall TUserOrderForm::ProcButtonClick(TObject *Sender) {
	DM->ADOStoredProcSum->Close();
	DM->ADOStoredProcSum->Parameters->ParamByName("id_cl")->Value =
		DM->ADOTClient->FieldByName("id")->AsString;
	DM->ADOStoredProcSum->Prepared = true;
	DM->ADOStoredProcSum->ExecProc();
	DM->ADOStoredProcSum->Open();
	// ---------------------------------------------------------------------------
}

// убрать фильтр, всё почистить
void __fastcall TUserOrderForm::FormClick(TObject *Sender) {
	DM->ADOStoredProcSum->Close();
	DM->ADOTClient->Filtered = false;
	ePoisk->Clear();
	ePoisk2->Clear();
	ePoisk3->Clear();
}
// ---------------------------------------------------------------------------

// поиск по имени
void __fastcall TUserOrderForm::ePoisk2Change(TObject *Sender) {
	if (ePoisk2->Text != "") {
		DM->ADOTClient->Filter = "first_name LIKE '" + ePoisk2->Text + "*'";
		DM->ADOTClient->Filtered = true;
	}
}
// ---------------------------------------------------------------------------

// поиск по отчеству
void __fastcall TUserOrderForm::ePoisk3Change(TObject *Sender) {
	if (ePoisk3->Text != "") {
		DM->ADOTClient->Filter = "middle_name LIKE '" + ePoisk3->Text + "*'";
		DM->ADOTClient->Filtered = true;
	}
}
// ---------------------------------------------------------------------------
