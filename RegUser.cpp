// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "RegUser.h"
#include "All_Connections_Invis.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TRegForm *RegForm;

// ---------------------------------------------------------------------------
__fastcall TRegForm::TRegForm(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TRegForm::FormCreate(TObject *Sender) {
	DM->ADOTClient->Open();
}

// ---------------------------------------------------------------------------
void __fastcall TRegForm::InsBClick(TObject *Sender) {
	if ((DBLName->Text.Length() == 0)) {
		ShowMessage("Не все поля заполнены");
	}
	else {
		DM->ADOTClient->Insert();
	}
	// TODO Хотя бы генерацию паролей для юзверей...
	Application->MessageBoxW(L"Пароль для входа:\nHgnTx5Wt ", MB_OK);

}

// ---------------------------------------------------------------------------
void __fastcall TRegForm::FormShow(TObject *Sender) {
	// Костыль? Упорно оставляет не очищенное поле без этого
	DM->ADOTClient->Insert();
}
// ---------------------------------------------------------------------------
