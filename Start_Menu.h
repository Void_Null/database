//---------------------------------------------------------------------------

#ifndef Start_MenuH
#define Start_MenuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
class TStartForm : public TForm
{
__published:	// IDE-managed Components
	TButton *UserButton;
	TButton *AdminButton;
	TButton *ButtonReg;
	TLabel *Label1;
	void __fastcall AdminButtonClick(TObject *Sender);
	void __fastcall UserButtonClick(TObject *Sender);
	void __fastcall FormClick(TObject *Sender);
	void __fastcall ButtonRegClick(TObject *Sender);
	void __fastcall FormDblClick(TObject *Sender);
private:	// User declarations
public:	  	// User declarations
	__fastcall TStartForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TStartForm *StartForm;
//---------------------------------------------------------------------------
#endif
