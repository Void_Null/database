// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UserLogIn.h"
#include "AsUserForm.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TUserPasswordForm *UserPasswordForm;

// ---------------------------------------------------------------------------
__fastcall TUserPasswordForm::TUserPasswordForm(TComponent* Owner)
	: TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TUserPasswordForm::BtnOKClick(TObject *Sender) {
	if (ePassUs->Text == "HgnTx5Wt") {
		UserOrderForm->ShowModal();
		UserPasswordForm->Close();
	}
	else {
		ShowMessage(
			"Пароль неверный\nПопробуйте ещё раз");
	}
}
// ---------------------------------------------------------------------------
