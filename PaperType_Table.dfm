object PTForm: TPTForm
  Left = 0
  Top = 0
  Caption = 'Paper Type'
  ClientHeight = 201
  ClientWidth = 369
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBNavigator1: TDBNavigator
    Left = 0
    Top = 0
    Width = 369
    Height = 25
    DataSource = DM.DataSource4
    Align = alTop
    TabOrder = 0
    ExplicitWidth = 349
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 25
    Width = 369
    Height = 176
    Align = alClient
    DataSource = DM.DataSource4
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Alignment = taLeftJustify
        Expanded = False
        FieldName = 'id_pt'
        Title.Caption = 'id'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'paper_type'
        Title.Caption = #1058#1080#1087' '#1073#1091#1084#1072#1075#1080
        Width = 150
        Visible = True
      end
      item
        Alignment = taLeftJustify
        Expanded = False
        FieldName = 'price'
        Title.Caption = #1062#1077#1085#1072
        Width = 150
        Visible = True
      end>
  end
end
