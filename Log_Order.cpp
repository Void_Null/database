// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Log_Order.h"
#include "All_Connections_Invis.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TLogOrdForm *LogOrdForm;

// ---------------------------------------------------------------------------
__fastcall TLogOrdForm::TLogOrdForm(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TLogOrdForm::FormShow(TObject *Sender) {
	DM->ADOLogOrder->Open();
}

// ---------------------------------------------------------------------------
void __fastcall TLogOrdForm::FormClose(TObject *Sender, TCloseAction &Action) {
	DM->ADOLogOrder->Close();
}
// ---------------------------------------------------------------------------
