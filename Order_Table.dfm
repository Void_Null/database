object Order_adm: TOrder_adm
  Left = 0
  Top = 0
  Caption = 'Order (Administrator)'
  ClientHeight = 201
  ClientWidth = 394
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBNavigator1: TDBNavigator
    Left = 0
    Top = 0
    Width = 394
    Height = 25
    DataSource = DM.DataSource5
    Align = alTop
    TabOrder = 0
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 25
    Width = 394
    Height = 176
    Align = alClient
    DataSource = DM.DataSource5
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'id'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'id_clients'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'id_paper'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'id_size'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'id_filial'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'kolvo'
        Visible = True
      end>
  end
end
