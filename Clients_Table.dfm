object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Client'
  ClientHeight = 262
  ClientWidth = 788
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 25
    Width = 788
    Height = 237
    Align = alClient
    DataSource = DM.DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Alignment = taLeftJustify
        Expanded = False
        FieldName = 'id'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'last_name'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'first_name'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'middle_name'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'phone_number'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'email'
        Width = 150
        Visible = True
      end>
  end
  object DBNavigator1: TDBNavigator
    Left = 0
    Top = 0
    Width = 788
    Height = 25
    DataSource = DM.DataSource1
    Align = alTop
    TabOrder = 1
  end
end
