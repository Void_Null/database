// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "PaperType_Table.h"
#include "All_Connections_Invis.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TPTForm *PTForm;

// ---------------------------------------------------------------------------
__fastcall TPTForm::TPTForm(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TPTForm::FormShow(TObject *Sender) {
	DM->ADOPaper_type->Open();
}
// ---------------------------------------------------------------------------
