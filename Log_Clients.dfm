object OSAForm: TOSAForm
  Left = 0
  Top = 0
  Caption = 'Log Clients'
  ClientHeight = 201
  ClientWidth = 361
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 25
    Width = 361
    Height = 176
    Align = alClient
    DataSource = DM.DataSource6
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Alignment = taLeftJustify
        Expanded = False
        FieldName = 'id_log'
        Visible = True
      end
      item
        Alignment = taLeftJustify
        Expanded = False
        FieldName = 'rowid'
        Title.Caption = 'row_id'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'typei'
        Title.Caption = 'type'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'time'
        Visible = True
      end>
  end
  object DBNavigator1: TDBNavigator
    Left = 0
    Top = 0
    Width = 361
    Height = 25
    DataSource = DM.DataSource6
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
    Align = alTop
    TabOrder = 1
  end
end
