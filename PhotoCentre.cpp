//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
USEFORM("PaperType_TableForClients.cpp", PTCform);
USEFORM("PaperType_Table.cpp", PTForm);
USEFORM("PaperSize_TableForClients.cpp", PSCform);
USEFORM("PaperSize_Table.cpp", PS);
USEFORM("RegUser.cpp", RegForm);
USEFORM("All_Connections_Invis.cpp", DM); /* TDataModule: File Type */
USEFORM("Clients_Table.cpp", Form3);
USEFORM("Start_Menu.cpp", StartForm);
USEFORM("Admin_Form.cpp", Form1);
USEFORM("Filial_Table.cpp", FilialForm);
USEFORM("LogIn.cpp", LoginForm);
USEFORM("AsUserForm.cpp", UserOrderForm);
USEFORM("Filials_TableForClients.cpp", FCform);
USEFORM("Log_Order.cpp", LogOrdForm);
USEFORM("Order_Table.cpp", Order_adm);
USEFORM("Log_Clients.cpp", OSAForm);
USEFORM("UserLogIn.cpp", UserPasswordForm);
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
	try
	{
		Application->Initialize();
		Application->MainFormOnTaskBar = true;
		Application->CreateForm(__classid(TStartForm), &StartForm);
		Application->CreateForm(__classid(TForm1), &Form1);
		Application->CreateForm(__classid(TDM), &DM);
		Application->CreateForm(__classid(TForm3), &Form3);
		Application->CreateForm(__classid(TUserOrderForm), &UserOrderForm);
		Application->CreateForm(__classid(TPS), &PS);
		Application->CreateForm(__classid(TPTForm), &PTForm);
		Application->CreateForm(__classid(TFilialForm), &FilialForm);
		Application->CreateForm(__classid(TOrder_adm), &Order_adm);
		Application->CreateForm(__classid(TRegForm), &RegForm);
		Application->CreateForm(__classid(TPTCform), &PTCform);
		Application->CreateForm(__classid(TPSCform), &PSCform);
		Application->CreateForm(__classid(TFCform), &FCform);
		Application->CreateForm(__classid(TLoginForm), &LoginForm);
		Application->CreateForm(__classid(TOSAForm), &OSAForm);
		Application->CreateForm(__classid(TLogOrdForm), &LogOrdForm);
		Application->CreateForm(__classid(TUserPasswordForm), &UserPasswordForm);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
