//---------------------------------------------------------------------------

#ifndef UserLogInH
#define UserLogInH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
class TUserPasswordForm : public TForm
{
__published:	// IDE-managed Components
	TEdit *ePassUs;
	TLabel *Label;
	TButton *BtnOK;
	void __fastcall BtnOKClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TUserPasswordForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TUserPasswordForm *UserPasswordForm;
//---------------------------------------------------------------------------
#endif
