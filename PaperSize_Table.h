//---------------------------------------------------------------------------

#ifndef PaperSize_TableH
#define PaperSize_TableH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.DBCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.Grids.hpp>
//---------------------------------------------------------------------------
class TPS : public TForm
{
__published:	// IDE-managed Components
	TDBNavigator *DBNavigator2;
	TDBGrid *DBGrid1;
	void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TPS(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TPS *PS;
//---------------------------------------------------------------------------
#endif
