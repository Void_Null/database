//---------------------------------------------------------------------------

#ifndef Filials_TableForClientsH
#define Filials_TableForClientsH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.Grids.hpp>
//---------------------------------------------------------------------------
class TFCform : public TForm
{
__published:	// IDE-managed Components
	TDBGrid *DBGrid1;
private:	// User declarations
public:		// User declarations
	__fastcall TFCform(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFCform *FCform;
//---------------------------------------------------------------------------
#endif
