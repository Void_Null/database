// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Start_Menu.h"
#include "RegUser.h"
#include "UserLogIn.h"
#include "Admin_Form.h"
#include "LogIn.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TStartForm *StartForm;

// ---------------------------------------------------------------------------
__fastcall TStartForm::TStartForm(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TStartForm::AdminButtonClick(TObject *Sender) {
	LoginForm->ShowModal();
}
// ---------------------------------------------------------------------------

void __fastcall TStartForm::UserButtonClick(TObject *Sender) {
	UserPasswordForm->ShowModal();
}
// ---------------------------------------------------------------------------

void __fastcall TStartForm::FormClick(TObject *Sender) {
	StartForm->AdminButton->Show();
}

void __fastcall TStartForm::ButtonRegClick(TObject *Sender) {
	RegForm->ShowModal();
}
// ---------------------------------------------------------------------------

void __fastcall TStartForm::FormDblClick(TObject *Sender) {
	StartForm->AdminButton->Hide();    //спрятали админскую кнопку :р
}
// ---------------------------------------------------------------------------
