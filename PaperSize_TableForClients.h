//---------------------------------------------------------------------------

#ifndef PaperSize_TableForClientsH
#define PaperSize_TableForClientsH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.Grids.hpp>
//---------------------------------------------------------------------------
class TPSCform : public TForm
{
__published:	// IDE-managed Components
	TDBGrid *DBGrid1;
private:	// User declarations
public:		// User declarations
	__fastcall TPSCform(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TPSCform *PSCform;
//---------------------------------------------------------------------------
#endif
