object StartForm: TStartForm
  Left = 0
  Top = 0
  Caption = #1060#1086#1090#1086#1089#1072#1083#1086#1085
  ClientHeight = 159
  ClientWidth = 201
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClick = FormClick
  OnDblClick = FormDblClick
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 110
    Width = 173
    Height = 13
    Caption = '('#1044#1086#1089#1090#1091#1087#1085#1086' '#1090#1086#1083#1100#1082#1086'  '#1076#1083#1103' '#1050#1083#1080#1077#1085#1090#1086#1074')'
    WordWrap = True
  end
  object UserButton: TButton
    Left = 24
    Top = 79
    Width = 153
    Height = 25
    Caption = #1054#1092#1086#1088#1084#1080#1090#1100' '#1079#1072#1082#1072#1079
    TabOrder = 0
    WordWrap = True
    OnClick = UserButtonClick
  end
  object AdminButton: TButton
    Left = 24
    Top = 129
    Width = 153
    Height = 25
    Caption = #1042#1086#1081#1090#1080' '#1082#1072#1082' '#1040#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1086#1088
    TabOrder = 1
    Visible = False
    OnClick = AdminButtonClick
  end
  object ButtonReg: TButton
    Left = 24
    Top = 32
    Width = 153
    Height = 25
    Caption = #1057#1090#1072#1090#1100' '#1085#1072#1096#1080#1084' '#1050#1083#1080#1077#1085#1090#1086#1084'...'
    TabOrder = 2
    OnClick = ButtonRegClick
  end
end
