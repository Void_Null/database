object PS: TPS
  Left = 0
  Top = 0
  Caption = 'Paper Size'
  ClientHeight = 393
  ClientWidth = 270
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBNavigator2: TDBNavigator
    Left = 0
    Top = 0
    Width = 270
    Height = 25
    DataSource = DM.DataSource3
    Align = alTop
    TabOrder = 0
    ExplicitWidth = 250
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 25
    Width = 270
    Height = 368
    Align = alClient
    DataSource = DM.DataSource3
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'id_ps'
        Title.Caption = 'id'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'size'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'price'
        Width = 100
        Visible = True
      end>
  end
end
