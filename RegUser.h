//---------------------------------------------------------------------------

#ifndef RegUserH
#define RegUserH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.DBCtrls.hpp>
#include <Vcl.Mask.hpp>
//---------------------------------------------------------------------------
class TRegForm : public TForm
{
__published:	// IDE-managed Components
	TDBEdit *DBFName;
	TLabel *Label1;
	TDBEdit *DBMName;
	TDBEdit *DBLName;
	TLabel *Label2;
	TLabel *Label3;
	TDBEdit *DBNumber;
	TLabel *Label4;
	TDBEdit *DBEmail;
	TLabel *Label5;
	TButton *InsB;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall InsBClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TRegForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TRegForm *RegForm;
//---------------------------------------------------------------------------
#endif
