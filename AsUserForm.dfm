object UserOrderForm: TUserOrderForm
  Left = 0
  Top = 0
  Caption = #1057#1076#1077#1083#1072#1090#1100' '#1079#1072#1082#1072#1079
  ClientHeight = 545
  ClientWidth = 430
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClick = FormClick
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 21
    Top = 282
    Width = 66
    Height = 16
    Caption = #1058#1080#1087' '#1073#1091#1084#1072#1075#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 21
    Top = 317
    Width = 76
    Height = 16
    Caption = #1056#1072#1079#1084#1077#1088' '#1092#1086#1090#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 24
    Top = 353
    Width = 45
    Height = 16
    Caption = #1060#1080#1083#1080#1072#1083
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 21
    Top = 401
    Width = 139
    Height = 16
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1086#1090#1087#1077#1095#1072#1090#1082#1086#1074
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 24
    Top = 17
    Width = 141
    Height = 16
    Caption = #1053#1072#1081#1090#1080' '#1089#1077#1073#1103' '#1087#1086' '#1092#1072#1084#1080#1083#1080#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 24
    Top = 57
    Width = 124
    Height = 16
    Caption = #1053#1072#1081#1090#1080' '#1089#1077#1073#1103' '#1087#1086' '#1080#1084#1077#1085#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 24
    Top = 97
    Width = 140
    Height = 16
    Caption = #1053#1072#1081#1090#1080' '#1089#1077#1073#1103' '#1087#1086' '#1086#1090#1095#1077#1089#1090#1074#1091
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object DBGrid1: TDBGrid
    Left = 21
    Top = 136
    Width = 388
    Height = 132
    DataSource = DM.DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'last_name'
        ReadOnly = True
        Title.Caption = #1060#1072#1084#1080#1083#1080#1103
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'first_name'
        ReadOnly = True
        Title.Caption = #1048#1084#1103
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'middle_name'
        ReadOnly = True
        Title.Caption = #1054#1090#1095#1077#1089#1090#1074#1086
        Width = 120
        Visible = True
      end>
  end
  object ePoisk: TEdit
    Left = 184
    Top = 16
    Width = 225
    Height = 21
    TabOrder = 1
    OnChange = ePoiskChange
  end
  object DBLookupPaperType: TDBLookupComboBox
    Left = 103
    Top = 282
    Width = 145
    Height = 21
    DataField = 'pt'
    DataSource = DM.DataSource5
    TabOrder = 2
  end
  object DBLookupPaperSize: TDBLookupComboBox
    Left = 103
    Top = 317
    Width = 145
    Height = 21
    DataField = 'ps'
    DataSource = DM.DataSource5
    TabOrder = 3
  end
  object DBLookupFilial: TDBLookupComboBox
    Left = 103
    Top = 352
    Width = 145
    Height = 21
    DataField = 'fil'
    DataSource = DM.DataSource5
    TabOrder = 4
  end
  object DBKolvo: TDBEdit
    Left = 184
    Top = 396
    Width = 64
    Height = 21
    DataField = 'kolvo'
    DataSource = DM.DataSource5
    TabOrder = 5
  end
  object OrderB: TButton
    Left = 296
    Top = 392
    Width = 113
    Height = 25
    Caption = #1057#1076#1077#1083#1072#1090#1100' '#1079#1072#1082#1072#1079
    TabOrder = 6
    OnClick = OrderBClick
  end
  object ButtonPT: TButton
    Left = 296
    Top = 281
    Width = 113
    Height = 21
    Caption = #1055#1086#1089#1084#1086#1090#1088#1077#1090#1100' '#1074#1089#1077
    TabOrder = 7
    OnClick = ButtonPTClick
  end
  object ButtonPS: TButton
    Left = 296
    Top = 316
    Width = 113
    Height = 21
    Caption = #1055#1086#1089#1084#1086#1090#1088#1077#1090#1100' '#1074#1089#1077
    TabOrder = 8
    OnClick = ButtonPSClick
  end
  object ButtonF: TButton
    Left = 296
    Top = 352
    Width = 113
    Height = 21
    Caption = #1055#1086#1089#1084#1086#1090#1088#1077#1090#1100' '#1074#1089#1077
    TabOrder = 9
    OnClick = ButtonFClick
  end
  object ProcButton: TButton
    Left = 160
    Top = 431
    Width = 105
    Height = 25
    Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1087#1077#1095#1072#1090#1080
    TabOrder = 10
    OnClick = ProcButtonClick
  end
  object DBGrid2: TDBGrid
    Left = 8
    Top = 471
    Width = 414
    Height = 66
    DataSource = DM.DataSource8
    TabOrder = 11
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'last_name'
        Title.Caption = #1060#1072#1084#1080#1083#1080#1103
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'first_name'
        Title.Caption = #1048#1084#1103
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'middle_name'
        Title.Caption = #1054#1090#1095#1077#1089#1090#1074#1086
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'itog'
        Title.Caption = #1050' '#1086#1087#1083#1072#1090#1077
        Width = 85
        Visible = True
      end>
  end
  object ePoisk2: TEdit
    Left = 184
    Top = 56
    Width = 225
    Height = 21
    TabOrder = 12
    OnChange = ePoisk2Change
  end
  object ePoisk3: TEdit
    Left = 184
    Top = 96
    Width = 225
    Height = 21
    TabOrder = 13
    OnChange = ePoisk3Change
  end
end
