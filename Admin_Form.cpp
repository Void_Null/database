﻿// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Admin_Form.h" //админская форма. Можно вводить свои запросы
#include "Clients_Table.h"  //Просто таблица клиентов
#include "All_Connections_Invis.h"  //невидимая форма, все подключаемые компоненты, в тч ADOConnection
#include "AsUserForm.h"   //таблица заказа для зарегестрированного клиента
#include "PaperSize_Table.h" //таблица с размером бумаги
#include "PaperType_Table.h" //таблица с типом бумаги
#include "Filial_Table.h"  //таблица филиалов
#include "Order_Table.h"  //просмотр
#include "Start_Menu.h"    //стартовое меню
#include "Log_CLients.h"   //логи изменений в таблице клиентов
#include "Log_Order.h"   //логи изменений в таблице заказов
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}
// ---------------------------------------------------------------------------

// Большое и красивое поле для самостоятельных запросов :3
void __fastcall TForm1::Button1Click(TObject *Sender) {
	DM->qZapros->Close();
	DM->qZapros->SQL->Clear();
	DM->qZapros->SQL->Text = MemoZapros->Text;
	DM->qZapros->ExecSQL();
	DM->qZapros->Open();
}
// ---------------------------------------------------------------------------

// показать
void __fastcall TForm1::AsUser1Click(TObject *Sender) {
	UserOrderForm->ShowModal();
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::PaperSize1Click(TObject *Sender) {
	PS->Show();
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::Clients1Click(TObject *Sender) {
	Form3->Show(); // таблица клиентов
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::PaperType1Click(TObject *Sender) {

	PTForm->Show(); // таблица типов бумаги
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::Filial1Click(TObject *Sender) {
	FilialForm->Show(); // таблица филиалов
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::Order1Click(TObject *Sender) {
	Order_adm->Show(); // таблица заказов
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::OrderSummary1Click(TObject *Sender) {
	OSAForm->Show(); // логи изменений в таблице клиентов
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::LogOrder1Click(TObject *Sender) {
	LogOrdForm->Show(); // логи изменений в таблице заказов
}
// ---------------------------------------------------------------------------
