// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "PaperSize_Table.h"
#include "All_Connections_Invis.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TPS *PS;

// ---------------------------------------------------------------------------
__fastcall TPS::TPS(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TPS::FormShow(TObject *Sender) {
	DM->ADOPaper_size->Open();
}
// ---------------------------------------------------------------------------
