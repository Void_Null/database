object FilialForm: TFilialForm
  Left = 0
  Top = 0
  Caption = 'Filial'
  ClientHeight = 201
  ClientWidth = 671
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBNavigator1: TDBNavigator
    Left = 0
    Top = 0
    Width = 671
    Height = 25
    DataSource = DM.DataSource2
    Align = alTop
    TabOrder = 0
    ExplicitWidth = 652
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 25
    Width = 671
    Height = 176
    Align = alClient
    DataSource = DM.DataSource2
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Alignment = taLeftJustify
        Expanded = False
        FieldName = 'id_f'
        Title.Caption = 'id'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'address'
        Title.Caption = #1040#1076#1088#1077#1089
        Width = 400
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'telephone_number'
        Title.Caption = #1058#1077#1083#1077#1092#1086#1085
        Width = 200
        Visible = True
      end>
  end
end
